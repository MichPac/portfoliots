

const MainSubMenu = () => {
    return (
        <div className="sub_container">
                <div>
                        <ul className="nav-links sub_menu">
                            <li><a href="#"><b>HTML</b></a></li>
                            <li><a href="#"><b>CSS</b></a></li>
                            <li><a href="#"><b>JAVASCRIPT</b></a></li>
                            <li><a href="#"><b>REACT</b></a></li>
                            <li><a href="#"><b>LOREM</b></a></li>
                        </ul>
                </div>
        </div>
    )
}

export default MainSubMenu
