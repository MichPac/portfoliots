import Png3 from './pngs/3.png'
import Png7 from './pngs/7.png'
import Png9 from './pngs/9.png'
import Png10 from './pngs/10.png'
import Png11 from './pngs/11.png'
import Png14 from './pngs/14.png'
import Png15 from './pngs/15.png'
import Png16 from './pngs/16.png'
import Png17 from './pngs/17.png'
import Png18 from './pngs/18.png'
import ProjOnePic from './pngs/boxA02.png'
import ProjTwoPic from './pngs/KV_radosc_1.png'
import ProjThreePic from './pngs/Screening1.png'

const pngComponents = {
    Png3,Png7,Png9,Png10,Png11,Png14,Png15,Png16,Png17,Png18,ProjOnePic,ProjTwoPic,ProjThreePic
}

export default pngComponents
