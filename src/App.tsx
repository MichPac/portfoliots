import './styles/themes/default/theme.scss'
import Homepage from './pages/homepage';

function App() {
  return (
    <div className="App">
      <Homepage/>
    </div>
  );
}

export default App;
