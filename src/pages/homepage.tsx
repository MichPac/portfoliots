
import Footer from '../components/Footer'
import {useEffect,useState} from 'react';
import Header from '../components/Header'
import Main from '../components/Main'


function Homepage() {

    const [state, setState] = useState(true)

    useEffect(() => {
       setState(false)
    }, [])

    return (
        <div>
            <div className="body-first__child">
            <Header/>
            <Main {...{state}}/>
            </div>
            <div className="body-second__child col-13">
                <Footer/>
            </div>
        </div>
    )
}

export default Homepage
