import FooterDown from '../elements/footerEl/FooterDown';
import FooterForm from '../elements/footerEl/FooterForm';

function Footer() {

    

    return (
        <footer className="pos-fixed">
            <div className="main-footer__container">
                <div className="main-footer__uppercontent">
                    <FooterForm/>
                </div>
                <div className="main-footer__downcontent">
                    <FooterDown/>
                </div>
            </div>
        </footer>
    )
}

export default Footer
