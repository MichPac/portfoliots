import React from 'react'
import pngComponents from '../assets/index';
import MainHi from '../elements/MainEl/MainHi';
import MainPreEl from '../elements/MainEl/MainPreEl';
import MainSubMenu from '../elements/MainEl/MainSubMenu';

interface State{
    state: boolean;
}

function Main(props:State) {
    const {Png3,Png14,Png16,ProjOnePic,ProjTwoPic,ProjThreePic} = pngComponents;


    return (
        <React.Fragment>
        <div className="upper-content">
            <img className="img one" src={Png14} alt="szlaczek"/>
            <img className="img two" src={Png14} alt="szlaczek"/>
            <img className="img three" src={Png16} alt="szlaczek"/>
            <img className="img" src={Png16} alt="szlaczek"/>
            <MainHi/>
            <div className="photo-background">
                <img className="facefoto" src={Png3} alt='FaceFoto'/>
            </div>
          </div>
        
          <div className="content-center__wrapper">
        <div className="center-content">
        <div id="table" className={ props.state ? "content-center__backgroundImg" : "content-center__backgroundImgAnimate" }>
            <div className="upper_bar">
                            <div className="dots">
                                <div className="dot-red"></div>
                                <div className="dot-yellow"></div>
                                <div className="dot-green"></div>
                            </div>
                            <p>//About</p>
                        </div>
                        <div className="content-center__backgroundImgdownTxt">
                            <div className="About">
                                <p>//<span> About</span></p>
                                <div className="contnet-wrapper">
                                    <div className="leftSide">
                                        <p>&lt;/ <span>Me </span>&gt;</p>
                                        <div className="pre">
                                            <MainPreEl/>
                                        </div>
                                    </div>
                                    <div className="rightSide">
                                        <p>&lt; / <span>Skills </span>&gt;</p>
                                        <div className="Skills">
                                            <div className="flex-content__wrapper">
                                                <div className="flex-product__wrapper">
                                                    <div className="Skills-bar css">
                                                        <p>CSS</p>
                                                    </div>
                                                </div>
                                                <div className="flex-product__wrapper">
                                                    <p>80%</p>
                                                </div>
                                            </div>

                                            <div className="flex-content__wrapper">
                                                <div className="flex-product__wrapper">
                                                    <div className="Skills-bar html">
                                                        <p>HTML</p>
                                                    </div>
                                                </div>
                                                <div className="flex-product__wrapper">
                                                    <p>90%</p>
                                                </div>
                                            </div>

                                            <div className="flex-content__wrapper">
                                                <div className="flex-product__wrapper">
                                                    <div className="Skills-bar css">
                                                        <p>JAVASCRIPT</p>
                                                    </div>
                                                </div>
                                                <div className="flex-product__wrapper">
                                                    <p>80%</p>
                                                </div>
                                            </div>

                                            <div className="flex-content__wrapper">
                                                <div className="flex-product__wrapper">
                                                    <div className="Skills-bar html">
                                                        <p>HTML</p>
                                                    </div>
                                                </div>
                                                <div className="flex-product__wrapper">
                                                    <p>90%</p>
                                                </div>
                                            </div>

                                            <div className="flex-content__wrapper">
                                                <div className="flex-product__wrapper">
                                                    <div className="Skills-bar css">
                                                        <p>REACT</p>
                                                    </div>
                                                </div>
                                                <div className="flex-product__wrapper">
                                                    <p>80%</p>
                                                </div>
                                            </div>

                                            <div className="flex-content__wrapper">
                                                <div className="flex-product__wrapper">
                                                    <div className="Skills-bar js">
                                                        <p>JS</p>
                                                    </div>
                                                </div>
                                                <div className="flex-product__wrapper">
                                                    <p>60%</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
        </div>

        <div className="down_content">
            <MainSubMenu/>
            <div className="projects_container">
                <div className="projects_wrapper">
                    <div className="projects-pictures"><img src={ProjOnePic} alt="proejkt1"/></div>
                    <div className="projects-pictures"><img src={ProjTwoPic} alt="projekt2"/></div>
                    <div className="projects-pictures"><img src={ProjThreePic} alt="proejkt3"/></div>
                </div>
                <div className="projects_wrapper column2">
                    <div className="projects-pictures"><img src={ProjTwoPic} alt="proejkt4"/></div>
                </div>
                <div className="line">
                    <div className="Contact">
                    <h2><span>//</span>Contact</h2>
                    </div>
                </div>
            </div>
        </div>
        </React.Fragment>
    )
}

export default Main
